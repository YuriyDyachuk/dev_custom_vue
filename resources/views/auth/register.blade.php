<!DOCTYPE html>
<html>
<head>
    <title>Animated Login Form</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a81368914c.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<a href="/" class="header_top"><h3>Главная</h3></a>
<img class="wave" src="{{ asset('img/wave.png') }}">
<div class="container">
    <div class="img">
        <img src="{{ asset('img/undraw_project.svg') }}">
    </div>
    <div class="login-content">
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <img src="{{ asset('img/undraw_profile.svg') }}">
            <h2 class="title">Register</h2>
            <div class="input-div one">
                <div class="i">
                    <i class="fa fa-user-plus"></i>
                </div>
                <div class="div">
                    <h5>Surname</h5>
                    <input id="surname" type="text" class="input form-control @error('surname') is-invalid @enderror"
                           name="surname" value="{{ old('surname') }}" required autocomplete="surname" autofocus>

                    @error('surname')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="input-div one">
                <div class="i">
                    <i class="fa fa-user-plus"></i>
                </div>
                <div class="div">
                    <h5>Name</h5>
                    <input id="name" type="text" class="input form-control @error('name') is-invalid @enderror"
                           name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="input-div one">
                <div class="i">
                    <i class="fa fa fa-phone"></i>
                </div>
                <div class="div">
                    <h5>Phone</h5>
                    <input id="phone" type="text" class="input form-control @error('phone') is-invalid @enderror"
                           name="phone"
                           value="{{ old('phone') }}" required autocomplete="phone" autofocus>

                    @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="input-div one">
                <div class="i">
                    <i class=" fa fa-reply"></i>
                </div>
                <div class="div">
                    <h5>E-mail</h5>
                    <input id="email" type="email" class="input form-control @error('email') is-invalid @enderror"
                           name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>

            <div class="input-div one">
                <div class="i">
                    <i class=" fa fa-parol"></i>
                </div>
                <div class="div">
                    <h5>Password</h5>
                    <input id="password" type="password" class="input form-control @error('password') is-invalid
@enderror" name="password" required autocomplete="new-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>

            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Регистрация') }}
                </button>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
</body>
</html>
