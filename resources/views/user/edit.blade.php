@extends('layouts.app')

@section('content')
<!-- Main content -->
<section class="container-fluid">
    <div class="row">
        <div class="offset-3 col-md-6">
            <div class="box">
                <form action="{{ route('home.users.update', $user->id) }}" enctype="multipart/form-data" method="POST"
                      data-toggle="validator">
                    @method('PUT')
                    @csrf
                    <div class="box-body">

                        <div class="form-group has-feedback">
                            <label for="surname">Фамилия</label>
                            <input type="text" class="form-control" name="surname" id="surname"
                                   value="@if(old('surname')){{old('surname')}}@else{{$user->surname ?? ''}} @endif" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="name">Имя</label>
                            <input type="text" class="form-control" name="name" id="name"
                                   value="@if(old('name')){{old('name')}}@else{{$user->name ?? ''}} @endif" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="phone">Телефон</label>
                            <input type="text" class="form-control" name="phone" id="phone"
                                   value="@if(old('phone')){{old('phone')}}@else{{$user->phone ?? ''}} @endif" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email"
                                   value="@if(old('email')){{old('email')}}@else{{$user->email ?? ''}} @endif" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                        <div class="form-group">
                            <label for="">Пароль</label>
                            <input type="text" class="form-control" name="password"
                                   placeholder="Введите пароль,если хотите его изменить">
                        </div>
                        <div class="form-group">
                            <label for="">Подтверждение пароля</label>
                            <input type="text" class="form-control" name="password_confirmation"
                                   placeholder="Подтверждение пароля">
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" name="file" class="custom-file-input">
                                <label class="custom-file-label" for="inputGroupFile03">Загрузить фото профиля</label>
                            </div>
                        </div>

                        <div class="box-footer">
                            <input type="hidden" name="id" value="{{ $user->id }}">
                            <button type="submit" class="btn btn-success">Сохранить</button>
                        </div>
                    </div>
                </form>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
