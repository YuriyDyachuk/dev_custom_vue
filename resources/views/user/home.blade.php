@extends('layouts.app')

@section('content')
    <div class="container emp-profile">
        <div class="row">
            <div class="col-md-4">
                <div class="profile-img">
                    @if (!empty($user->avatar))
                        <img src="{{ asset('profile_avatar/' . $user->avatar ) }}" alt="Profile"/>
                    @else
                        <img src="{{ asset('img/user_avatar.png') }}" alt="Avatar">
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="profile-head">
                    <h6>{{$user->name}}</h6>
                    <h5>{{$user->surname}}</h5>
                    <br>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" role="tab"
                               aria-controls="home" aria-selected="true">Профиль</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <a href="{{ route('home.users.edit', $user->id) }}"
                   title="Редактировать профиль">
                    <i class="btn btn-xs"></i>
                    <button type="submit" class="btn btn-success
                                                btn-xs">Редактировать</button>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="profile-work">
                    <p>WORK LINK</p>
                    <a href="">Website Link</a><br/>
                    <p>SKILLS</p>
                    <a href="">Web Designer</a><br/>
                </div>
            </div>
            <div class="col-md-8">
                <div class="tab-content profile-tab" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Фамилия</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$user->surname}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Имя</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$user->name}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>E-mail</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$user->email}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Номер телефона</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$user->phone}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
