<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    public function index()
    {
        $id = auth()->user()->id;
        $user = User::find($id);
        return view('user.home',['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('user.edit',['user' => $user]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $file = $request->file('file');
        $image_name = date('Y-m-d') . '.' . $file->getClientOriginalExtension();
        // перемещаем в указаную папку
        $destinationPath = public_path('profile_avatar\\');
        $destinationPathSmall = public_path('img_small_profile\\');
        $resize_image = Image::make($file->getRealPath());
        $resize_image->resize(250, 250, function($constraint){
            $constraint->aspectRatio();
        })->save($destinationPathSmall  . '/' . $image_name);

        if ($file) {
            $file->move($destinationPath, $file->getClientOriginalName());
        }

        $user->surname = $request['surname'];
        $user->name = $request['name'];
        $user->phone = $request['phone'];
        $user->email = $request['email'];
        if (empty($user->avatar)) {
            $user->avatar = $file->getClientOriginalName();
        }
        $request['password'] == null ?: $user->password = bcrypt($request['password']);
        $user->save();

        if (!$user) {
            return back()
                ->withErrors(['msg' => 'Ошибка сохранения'])
                ->withInput();
        } else {
            return redirect()
                ->route('home.users.index')
                ->with(['success' => 'Успешно сохранено']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
