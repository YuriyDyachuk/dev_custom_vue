<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as ImageInt;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function upload(Request $request)
    {
        $file = $request->file('file');
        // отображаем имя файла
        $name = $file->getClientOriginalName();
        //отображаем расширение файла
        $file->getClientOriginalExtension();
        //отображаем фактический путь к файлу
        $file->getRealPath();
        //отображаем размер файла
        $file->getSize();
        //отображаем Mime-тип файла
        $file->getMimeType();
        //перемещаем загруженный файл
        $destinationPath = 'profile_avatar';
        $file->move($destinationPath,$file->getClientOriginalName());

        $id = auth()->user()->id;
        $user_profile = User::find($id);
        $user_profile->avatar = $name;
        $user_profile->save();

        return redirect()->route('home');
    }
}
